# Suggested crontab line:
# */2 * * * * /bin/bash ssh-keys/update.sh 2>/dev/null > /dev/null

cd ssh-keys
git pull
cd
(cat .ssh/authorized_keys ssh-keys/ubuntu/*.pub | sort | uniq > .ssh/new_authorized_keys) && mv .ssh/new_authorized_keys .ssh/authorized_keys
chmod 644 .ssh/authorized_keys
